Commerce CCBill
This project integrates CCBill into the http://drupal.org/project/commerce payment and checkout systems. Allowing you to make online payments using this gateway. After receiving payment, commerce payment entity is created and attached to proper order.

REQUIREMENTS

This module requires the following modules:
* Commerce2 (https://drupal.org/project/commerce)

CONFIGURATION
* Configure Flexform payment flow
* Configure notification Webhook - section Enabling Webhooks in the Admin Portal (default notify url: /payment/notify/{payment_name})
* Please Add "CCBill" Payment gateway under
"admin/commerce/config/payment-gateways/add" using CCBill Plugin.
* Add the required credentials.

DEVELOPERS

For developers, the module fires a CCBILL_PAYMENT_RECEIVED event after payment is received, which can be used to trigger additional actions.

Overall, this module provides a convenient and secure way to process online payments using CCBill, and the ability for developers to customize and extend its functionality through event triggering.