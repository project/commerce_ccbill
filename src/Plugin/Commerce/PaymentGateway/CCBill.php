<?php

namespace Drupal\commerce_ccbill\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_ccbill\Event\CCBillPaymentEvent;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the CCBill payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "ccbill",
 *   label = "CCBill",
 *   display_label = "CCBill",
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_ccbill\PluginForm\CCBillPaymentForm",
 *   },
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Live"),
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class CCBill extends OffsitePaymentGatewayBase {

  const SECURE_IP_RANGES = [
    ['64.38.212.1', '64.38.212.254'],
    ['64.38.215.1', '64.38.215.254'],
    ['64.38.240.1', '64.38.240.254'],
    ['64.38.241.1', '64.38.241.254'],
  ];

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, LoggerChannelFactoryInterface $logger_channel_factory, EventDispatcherInterface $event_dispatcher, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->logger = $logger_channel_factory->get('commerce_ccbill');
    $this->eventDispatcher = $event_dispatcher;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('logger.factory'),
      $container->get('event_dispatcher'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'client_account_number' => '',
        'client_subaccount_number' => '',
        'flex_form_id' => '',
        'salt' => '',
        'validate_ip' => TRUE,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['client_account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Account Number'),
      '#default_value' => $this->configuration['client_account_number'],
      '#required' => TRUE,
    ];
    $form['client_subaccount_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Subaccount Number'),
      '#default_value' => $this->configuration['client_subaccount_number'],
      '#required' => TRUE,
    ];
    $form['flex_form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Flex Form ID'),
      '#default_value' => $this->configuration['flex_form_id'],
      '#required' => TRUE,
    ];
    $form['salt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Salt'),
      '#default_value' => $this->configuration['salt'],
      '#required' => TRUE,
    ];
    $form['validate_ip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validate IP'),
      '#default_value' => $this->configuration['validate_ip'],
      '#description' => $this->t('Validate IP address of the request.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['client_account_number'] = $values['client_account_number'];
      $this->configuration['client_subaccount_number'] = $values['client_subaccount_number'];
      $this->configuration['flex_form_id'] = $values['flex_form_id'];
      $this->configuration['salt'] = $values['salt'];
      $this->configuration['validate_ip'] = $values['validate_ip'];
    }
  }

  public function onNotify(Request $request) {
    if ($this->validateRequest($request)) {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $order = Order::load($request->get('X-order'));

      $event_type = $request->get('eventType');
      $transaction_id = $request->get('transactionId');

      $existing_payment = $payment_storage->loadByRemoteId($transaction_id);

      if ($event_type == 'NewSaleSuccess') {
        if (!$existing_payment) {
          $payment = $payment_storage->create([
            'state' => 'completed',
            'amount' => new Price((string) $request->get('accountingInitialPrice'), $request->get('accountingCurrency')),
            'payment_gateway' => $this->parentEntity->id(),
            'order_id' => $order->id(),
            'remote_id' => $transaction_id,
            'completed' => $this->time->getRequestTime(),
          ]);
          $payment->save();
          $order->set('state', 'completed');
          $order->save();

          $event = new CCBillPaymentEvent($payment);
          $this->eventDispatcher->dispatch(CCBillPaymentEvent::CCBILL_PAYMENT_RECEIVED, $event);
        }
      }
    }
    return new Response('TRUE', Response::HTTP_OK);
  }

  public function validateRequest(Request $request): bool {
    $request_form_digest = $request->get('X-formDigest');
    if ($this->configuration['validate_ip'] && !$this->isIpRangeValid($request->getClientIp())) {
      $this->logger->warning('Request is not from secure ip range');
      return FALSE;
    }
    if (!$this->isMd5Valid($request_form_digest, $request->get('subscriptionInitialPrice'), $request->get('initialPeriod'), $request->get('subscriptionCurrencyCode'))) {
      $this->logger->warning('MD5 is not valid');
      return FALSE;
    }
    return TRUE;
  }

  public function isMd5Valid(string $md5sum, string $initialPrice, string $initialPeriod, string $currencyCode): bool {
    $form_digest = md5($initialPrice . $initialPeriod . $currencyCode . $this->getConfiguration()['salt']);
    if ($form_digest === $md5sum) {
      return TRUE;
    }
    return FALSE;
  }

  public function isIpRangeValid(string $ip): bool {
    $ip_long = ip2long($ip);

    foreach (self::SECURE_IP_RANGES as $range) {
      if ($ip_long >= ip2long($range[0]) && $ip_long <= ip2long($range[1])) {
        return TRUE;
      }
    }

    return FALSE;
  }
}
