<?php

namespace Drupal\commerce_ccbill\PluginForm;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

const CCBILL_SANDBOX_URL = 'https://sandbox-api.ccbill.com/wap-frontflex/flexforms';
const CCBILL_LIVE_URL = 'https://api.ccbill.com/wap-frontflex/flexforms';

const CURRENCY_CODES = [
  'EUR' => '978',
  'AUD' => '036',
  'CAD' => '124',
  'GBP' => '826',
  'JPY' => '392',
  'USD' => '840',
];
class CCBillPaymentForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }


  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_ccbill\Plugin\Commerce\PaymentGateway\CCBill $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $configuration = $payment_gateway_plugin->getConfiguration();
    $order = $form_state->getFormObject()->getOrder();

    $data = $this->getData($order, $configuration);

    $form['#action'] = $this->getApiUrl($configuration);
    $form['#attributes']['target'] = '_blank';

    return $this->buildRedirectForm($form, $form_state, $form['#action'], $data, PaymentOffsiteForm::REDIRECT_GET);
  }

  public function getData(OrderInterface $order, Array $configuration): array {
    $data = [
      'clientSubacc' => $configuration['client_subaccount_number'],
      'clientAccnum' => $configuration['client_account_number'],
      'initialPrice' => number_format((float) $order->getTotalPrice()->getNumber(), 2, '.', ''),
      'initialPeriod' => '3',
      'currencyCode' => CURRENCY_CODES[$order->getTotalPrice()->getCurrencyCode()] ?? '',
      'email' => $order->getEmail(),
      'language' => $this->languageManager->getCurrentLanguage()->getId(),
      'order' => $order->id(),
    ];

    $data['formDigest'] = md5($data['initialPrice'] . $data['initialPeriod'] . $data['currencyCode'] . $configuration['salt']);

    if ($profile = $order->getBillingProfile()) {
      $data['customer_fname'] = $profile->get('address')->given_name;
      $data['customer_lname'] = $profile->get('address')->family_name;
      $data['address1'] = $profile->get('address')->address_line1;
      $data['city'] = $profile->get('address')->locality;
      $data['state'] = $profile->get('address')->administrative_area;
      $data['zipcode'] = $profile->get('address')->postal_code;
      $data['country'] = $profile->get('address')->country_code;
    }

    return $data;
  }

  public function getApiUrl(Array $configuration): string {
    if ($configuration['mode'] == 'live') {
      $api_url = CCBILL_LIVE_URL;
    }
    else {
      $api_url = CCBILL_SANDBOX_URL;
    }
    return "$api_url/{$configuration['flex_form_id']}";
  }


}
